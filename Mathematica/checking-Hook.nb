(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2782,         95]
NotebookOptionsPosition[      2237,         76]
NotebookOutlinePosition[      2612,         93]
CellTagsIndexPosition[      2569,         90]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{
    FractionBox["1", "2"], 
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox[
       RowBox[{"q", " ", "fa"}], 
       RowBox[{"4", "\[Pi]", " ", "Mpl"}]], ")"}], "2"], 
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox["RNS", "MNS"], ")"}], "2"]}], "/.", 
   RowBox[{"{", 
    RowBox[{"q", "\[Rule]", 
     RowBox[{"4", "\[Pi]", " ", "fa", " ", "RNS"}]}], "}"}]}], "/.", 
  RowBox[{"{", 
   RowBox[{"fa", "\[Rule]", 
    RowBox[{"1", "/", "faInv"}]}], "}"}]}], "\[IndentingNewLine]", 
 RowBox[{"faInvSol", "=", 
  RowBox[{"faInv", "/.", 
   RowBox[{
    RowBox[{"Solve", "[", 
     RowBox[{
      RowBox[{"%", "\[Equal]", "1"}], ",", "faInv"}], "]"}], "[", 
    RowBox[{"[", "4", "]"}], "]"}]}]}]}], "Input",
 CellChangeTimes->{{3.742995438976397*^9, 3.742995486682946*^9}, {
  3.742995555060711*^9, 3.742995635409093*^9}},
 CellLabel->
  "In[333]:=",ExpressionUUID->"f36cad11-bd1d-4315-98ab-c4ced4eae993"],

Cell[BoxData[
 FractionBox[
  SuperscriptBox["RNS", "4"], 
  RowBox[{"2", " ", 
   SuperscriptBox["faInv", "4"], " ", 
   SuperscriptBox["MNS", "2"], " ", 
   SuperscriptBox["Mpl", "2"]}]]], "Output",
 CellChangeTimes->{{3.7429956162026052`*^9, 3.742995635717815*^9}},
 CellLabel->
  "Out[333]=",ExpressionUUID->"6e4fb19d-b3fa-4513-9c95-ff1dbbddd9aa"],

Cell[BoxData[
 FractionBox["RNS", 
  RowBox[{
   SuperscriptBox["2", 
    RowBox[{"1", "/", "4"}]], " ", 
   SqrtBox["MNS"], " ", 
   SqrtBox["Mpl"]}]]], "Output",
 CellChangeTimes->{{3.7429956162026052`*^9, 3.742995635726961*^9}},
 CellLabel->
  "Out[334]=",ExpressionUUID->"f238f88e-4b3a-4826-8eca-bc088c0f75bb"]
}, Open  ]]
},
WindowSize->{808, 751},
WindowMargins->{{44, Automatic}, {-4, Automatic}},
Magnification->1.25,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 970, 29, 92, "Input",ExpressionUUID->"f36cad11-bd1d-4315-98ab-c4ced4eae993"],
Cell[1553, 53, 351, 9, 69, "Output",ExpressionUUID->"6e4fb19d-b3fa-4513-9c95-ff1dbbddd9aa"],
Cell[1907, 64, 314, 9, 73, "Output",ExpressionUUID->"f238f88e-4b3a-4826-8eca-bc088c0f75bb"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

