#!/bin/bash
#mv ./*.eps ./
for f in ./*.eps; do epstopdf $f;  done
mv ./*.pdf ../Image_Files/EPS_Files/Converted_Pdf/
mv ./*.eps ../Image_Files/EPS_Files/
git add ../Image_Files/EPS_Files/Converted_Pdf/*.pdf
git add ./*.agr
git add ../Image_Files/EPS_Files/*.eps
git commit -m "Updating plot pdf and agr"
git push
